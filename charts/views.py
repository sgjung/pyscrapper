from django.shortcuts import render
from django.template import loader
from django.http import HttpResponse


# Create your views here.
def index(request):
    template_data = {}
    return render(request, 'charts/index.html', template_data)


def render_chart(request):
    template_data = {
        'day': [1,2,3],
        'temperature': [29,27,33]
    }

    if request.method == 'GET':
        print("get...")

    return render(request, 'charts/chart.html', template_data)


def render_map_demo(request):
    context = {}
    return render(request, 'charts/map/demo.html', context)


def render_naver_points(request):
    context = {}
    return render(request, 'charts/map/naver_map.html', context)




