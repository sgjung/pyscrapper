from django.urls import path, re_path
from charts.views import *
# from django.conf.urls import url, include # django 1.x

urlpatterns = [
    path('', index, name='index'),
    re_path('chart_sample/', render_chart, name='chart_sample'),
    re_path('map/naver', render_naver_points, name='step_sample')
    # url(r'^$', views.index, name='index'),
    # url(r'^sample/$', views.render_chart, name='chart_sample')
]
