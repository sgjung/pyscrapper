"""scrapper URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, re_path
from django.http import HttpResponseRedirect

from .views import *
from home import views as root
# from django.conf.urls import url, include

# https://stackoverflow.com/questions/34587009/django-urls-py-root-path-set
# 아래는 1.x 버전대의 예인듯 하다.  별로 좋은 예는 아니었다.
# https://stackoverflow.com/questions/3092865/django-view-load-template-from-calling-apps-dir-first

urlpatterns = [
    # re_path('', root.index, name='home'), // index.html 을 뿌릴때...
    re_path('home', root.index, name='home'),
    path('admin/', admin.site.urls),
    path('charts/', include('charts.urls'))
    # url(r'^admin/', admin.site.urls),
    # url(r'^charts/', include('charts.urls', namespace='charts'))
]
